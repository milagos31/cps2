const User = require("../models/User");
const bcrypt = require("bcrypt");
const saltRounds = 10;
const auth = require("./../auth");
const Product = require("../models/Product");

//Get Users
module.exports.getUsers = () => {
    return User.find().then((result, err) => {
        if (err) {
            return err;
        } else {
            console.log("Retrieving users");
            return result;
        }
    });
};

//Register
module.exports.registerUser = (body) => {
    return User.find({ email: body.email }).then((result, err) => {
        if (err) {
            return err;
        } else if (result.length < 1) {
            body.password = bcrypt.hashSync(body.password, saltRounds);
            return new User(body).save().then((result, err) => {
                if (err) {
                    return err;
                } else {
                    console.log("New user registered", result.email);
                    return result;
                }
            });
        } else {
            return "Email already exist!";
        }
    });
};

//Login
module.exports.loginUser = (body) => {
    return User.findOne({ email: body.email }).then((result, err) => {
        const validPassword = bcrypt.compareSync(body.password, result.password);
        if (err) {
            return err;
        } else if (validPassword) {
            console.log("User successfully logged in", result.email);
            return { email: result.email, isAdmin: result.isAdmin, token: auth.createAccessToken(result) };
            // return result;
        } else {
            return "Invalid Password";
        }
    });
};

//Get User Orders (USER)
module.exports.getMyOrders = (id) => {
    return User.findById(id).then((result, err) => {
        if (err) {
            return err;
        } else {
            console.log("User retrieving orders");
            return result.orders;
        }
    });
};

//Get All Orders (ADMIN)
module.exports.getAllOrders = () => {
    return User.find({ orders: { $ne: [] } })
        .select("-password -isAdmin -__v")
        .then((result, err) => {
            if (err) {
                return err;
            } else {
                console.log("Retrieving all orders");
                return result;
            }
        });
};

//Checkout (USER)
module.exports.checkout = async (id, body) => {
    const newOrder = await User.findById(id).then((result, err) => {
        if (err) {
            return err;
        } else {
            result.orders.push(body);
            return result.save().then((result, err) => {
                if (err) {
                    console.log("Error on saving order");
                    return false;
                } else {
                    console.log("Done saving order to User:", result.email);
                    return result.orders[result.orders.length - 1];
                }
            });
        }
    });
    if (newOrder) {
        for (const order of newOrder.products) {
            await Product.findById(order.productId).then((result, err) => {
                if (err) {
                    return false;
                } else {
                    result.orders.push({ orderId: newOrder._id });
                    return result.save().then((result, err) => {
                        if (err) {
                            console.log("Error on saving order ID to the product");
                            return false;
                        } else {
                            console.log("Received", order.quantity, "order(s) of", result.name);
                            return true;
                        }
                    });
                }
            });
        }

        return newOrder;
    } else {
        return "Could not process your order";
    }
};

module.exports.setAsAdmin = (id) => {
    return User.findByIdAndUpdate(id, { isAdmin: true }, { returnDocument: "after" })
        .select("-password")
        .then((result, err) => {
            if (err) {
                return err;
            } else {
                console.log("Successfully added as Admin:", result.email);
                return result;
            }
        });
};

module.exports.changePassword = (id, body) => {
    return User.findById(id).then((result, err) => {
        const validPassword = bcrypt.compareSync(body.password, result.password);
        if (err) {
            return err;
        } else if (validPassword) {
            result.password = bcrypt.hashSync(body.newPassword, saltRounds);
            return result.save().then((result, err) => {
                if (err) {
                    return "Error on saving new password";
                } else {
                    console.log(result.email, "changed its password!");
                    return "New password has been saved successfully";
                }
            });
        }
    });
};
