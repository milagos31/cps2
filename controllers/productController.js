const { model } = require("mongoose");
const Product = require("../models/Product");

// Get All Products
module.exports.getAll = (isAdmin) => {
    let select = "-orders";
    if (isAdmin) select = null;

    return Product.find()
        .select(select)
        .then((result, err) => {
            if (err) {
                return err;
            } else {
                console.log("Getting all products:");
                return result;
            }
        });
};

// Get All Active Products
module.exports.getAllActive = () => {
    return Product.find({ isActive: true })
        .select("-orders")
        .then((result, err) => {
            if (err) {
                return err;
            } else {
                console.log("Retrieving active products:");
                return result;
            }
        });
};

// Add Product (ADMIN)
module.exports.addProduct = (body) => {
    return new Product(body).save().then((result, err) => {
        if (err) {
            return err;
        } else {
            console.log("Successfully added new product:", result.name);
            return result;
        }
    });
};

// Get Product by ID
module.exports.getProductById = (id) => {
    return Product.findById(id).then((result, err) => {
        if (err) {
            return err;
        } else {
            console.log("Retrieving product", id);
            return result;
        }
    });
};

// Update Product by ID
module.exports.updateProduct = (id, body) => {
    return Product.findByIdAndUpdate(id, body, { returnDocument: "after" }).then((result, err) => {
        if (err) {
            return err;
        } else {
            console.log("Successfully updated product:", result.name);
            return result;
        }
    });
};

// Archive Product
module.exports.archiveProduct = (id) => {
    return Product.findByIdAndUpdate(id, { isActive: false }, { returnDocument: "after" }).then((result, err) => {
        if (err) {
            return err;
        } else {
            console.log("Done archiving product:", result.name);
            return result;
        }
    });
};

// Activate Product
module.exports.activateProduct = (id) => {
    return Product.findByIdAndUpdate(id, { isActive: true }, { returnDocument: "after" }).then((result, err) => {
        if (err) {
            return err;
        } else {
            console.log("Done activating product:", result.name);
            return result;
        }
    });
};
