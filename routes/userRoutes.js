const router = require("express").Router();
const userController = require("../controllers/userController");
const auth = require("./../auth");

// Get Users
router.get("/", auth.verify, (req, res) => {
    const payload = auth.decode(req.headers.authorization);
    if (payload.isAdmin) {
        userController.getUsers().then((result) => res.send(result));
    } else {
        res.send("You are not authorized!");
    }
});

// Register
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then((result) => res.send(result));
});

// Login
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then((result) => res.send(result));
});

// Checkout (USER)
router.post("/checkout", auth.verify, (req, res) => {
    const payload = auth.decode(req.headers.authorization);
    if (!payload.isAdmin) {
        userController
            .checkout(payload.id, req.body)
            .then((result) => res.send({ email: payload.email, order: result }));
    } else {
        res.send("Please login as USER!");
    }
});

// Get Orders (USER)
router.get("/myOrders", auth.verify, (req, res) => {
    const payload = auth.decode(req.headers.authorization);
    if (!payload.isAdmin) {
        userController.getMyOrders(payload.id).then((result) => res.send(result));
    } else {
        res.send("Please login as USER!");
    }
});

// Get All Orders (ADMIN)
router.get("/orders", auth.verify, (req, res) => {
    const payload = auth.decode(req.headers.authorization);
    if (payload.isAdmin) {
        userController.getAllOrders().then((result) => res.send(result));
    } else {
        res.send("You are not authorized!");
    }
});

// Set User as ADMIN
router.put("/:id/setAsAdmin", auth.verify, (req, res) => {
    const payload = auth.decode(req.headers.authorization);
    if (payload.isAdmin) {
        userController.setAsAdmin(req.params.id).then((result) => res.send(result));
    } else {
        res.send("You are not authorized!");
    }
});

// Change password
router.put("/changePassword", auth.verify, (req, res) => {
    const payload = auth.decode(req.headers.authorization);
    userController.changePassword(payload.id, req.body).then((result) => res.send(result));
});
///////////////////////////////
module.exports = router;
