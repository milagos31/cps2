const router = require("express").Router();
const productController = require("../controllers/productController");
const auth = require("./../auth");

// Get All Products
router.get("/all", auth.verify, (req, res) => {
    const payload = auth.decode(req.headers.authorization);
    productController.getAll(payload.isAdmin).then((result) => res.send(result));
});

// Get All Active Products
router.get("/active", (req, res) => {
    productController.getAllActive().then((result) => res.send(result));
});

// Add Product (ADMIN)
router.post("/", auth.verify, (req, res) => {
    const payload = auth.decode(req.headers.authorization);
    if (payload.isAdmin) {
        productController.addProduct(req.body).then((result) => res.send(result));
    } else {
        res.send("Your are not authorized!");
    }
});

// Get Product by ID
router.get("/:id", auth.verify, (req, res) => {
    const payload = auth.decode(req.headers.authorization);
    if (payload.isAdmin) {
        productController.getProductById(req.params.id).then((result) => res.send(result));
    } else {
        res.send("Your are not authorized!");
    }
});

// Update Product (ADMIN)
router.put("/:id", auth.verify, (req, res) => {
    const payload = auth.decode(req.headers.authorization);
    if (payload.isAdmin) {
        productController.updateProduct(req.params.id, req.body).then((result) => res.send(result));
    } else {
        res.send("Your are not authorized!");
    }
});

// Archive Product (ADMIN)
router.put("/:id/archive", auth.verify, (req, res) => {
    const payload = auth.decode(req.headers.authorization);
    if (payload.isAdmin) {
        productController.archiveProduct(req.params.id).then((result) => res.send(result));
    } else {
        res.send("Your are not authorized!");
    }
});

// Activate Product (ADMIN)
router.put("/:id/activate", auth.verify, (req, res) => {
    const payload = auth.decode(req.headers.authorization);
    if (payload.isAdmin) {
        productController.activateProduct(req.params.id).then((result) => res.send(result));
    } else {
        res.send("Your are not authorized!");
    }
});
/////////////////////////////////////////////
module.exports = router;
