const express = require("express");
const app = express();
// const PORT = 3004;
const cors = require("cors");

const mongoose = require("mongoose");
const URI = "mongodb+srv://admin:admin1234@zuitt-bootcamp.inrmr.mongodb.net/cps2?retryWrites=true&w=majority";
mongoose.connect(URI, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.once("open", () => console.log("connected to MongoDB Atlas"));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const userRoutes = require("./routes/userRoutes");
app.use("/users", userRoutes);
const productRoutes = require("./routes/productRoutes");
app.use("/products", productRoutes);

app.get("/", (req, res) => {
    res.send("Welcome to My E-Commerce API");
});

app.listen(process.env.PORT || 3004, () => console.log("Server is running, catch it if you can!"));
